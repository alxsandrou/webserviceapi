package movil.accesaa.oitic.com.mx.boletera;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;

import movil.accesaa.oitic.com.mx.boletera.model.EmergenciaDTO;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
//@RunWith(AndroidJUnit4.class)
public class ExampleUnitTest  {
    @Test

    public void addition_isCorrect() {
       // assertEquals(4, 2 + 2);
        String url = "http://oitic.com.mx/boletera/api/concierto/valida/emergencia/18VC9CSNGE0606";
     /*   OkHttpClientHttpRequestFactory client = new OkHttpClientHttpRequestFactory();
        try {
            URI uri = new URI(url);
            ClientHttpRequest request = client.createRequest(uri, HttpMethod.GET);
                assertEquals("si", request.execute().toString());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }catch (IOException e) {
            e.printStackTrace();
        }
/*
       RestTemplate restTemplate = new RestTemplate(client);
        //   restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
         EmergenciaDTO[] arlCodigo = restTemplate.getForObject(url, EmergenciaDTO[].class);

        assertEquals(2, arlCodigo.length);   */


       // HttpUrl.Builder urlBuilder = HttpUrl.parse(url).newBuilder();
       // String ruta = urlBuilder.build().toString();
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .build();
        try {
            Response response = client.newCall(request).execute();

            Type listType = new TypeToken<List<EmergenciaDTO>>() {}.getType();
            List<EmergenciaDTO> yourList = new Gson().fromJson(response.body().string(), listType);

        //    assertEquals("si", response.body().string());

        //    JSONObject jsonObject = new JSONObject(response.body().string());

            assertEquals(2, yourList.size());



        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}