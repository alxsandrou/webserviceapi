package movil.accesaa.oitic.com.mx.boletera;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import movil.accesaa.oitic.com.mx.boletera.model.EmergenciaDTO;

public class DetalleActivity extends AppCompatActivity {



    private FloatingActionButton fab;
    String url = "http://oitic.com.mx/boletera/api/concierto/valida/emergencia" ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle);
        fab = findViewById(R.id.fab);


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                EmergenciaDTO[] arlCodigo = restTemplate.getForObject(url, EmergenciaDTO[].class);

                Toast.makeText(getApplicationContext(), "Leído: " + arlCodigo, Toast.LENGTH_LONG).show();

            }
        });


    }
}
