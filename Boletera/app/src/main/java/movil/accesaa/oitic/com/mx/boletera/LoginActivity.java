package movil.accesaa.oitic.com.mx.boletera;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;

import movil.accesaa.oitic.com.mx.boletera.model.PersonalSeguridadDTO;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


public class LoginActivity extends AppCompatActivity {

    private CardView dvwLogin,CardView2;
    private TextView lblRegistro;
    private EditText edtUsuario,edtPassword;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        dvwLogin = findViewById(R.id.dvwLogin);
        CardView2 = findViewById(R.id.CardView2);
        lblRegistro = findViewById(R.id.lblRegistro);
        edtUsuario = findViewById(R.id.edtUsuario);
        edtPassword = findViewById(R.id.edtPassword);
        edtUsuario.setText("");
        edtPassword.setText("");

        dvwLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String usuario = edtUsuario.getText().toString();
                String password = edtPassword.getText().toString();

                if(usuario.isEmpty() && password.isEmpty()){
                    Toast.makeText(getApplicationContext(),
                            "Campos requeridos",Toast.LENGTH_LONG).show();

                }else{
                    if(usuario.equals("admin") && password.equals("admin")) {
                        Intent intent = new Intent(v.getContext(),LecturaQRActivity.class);
                        startActivityForResult(intent,0);
                    }else{
                        Toast.makeText(getApplicationContext(),
                                "Datos incorrectos",Toast.LENGTH_LONG).show();
                    }

                }
            }
        });

        CardView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(),LoginGoogleActivity.class);
                startActivityForResult(intent,0);
            }
        });

        lblRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
  /*              Intent intent = new Intent(v.getContext(),RegistroActivity.class);
                startActivityForResult(intent,0);
*/
                enviarEmail();


            }
        });
    }
    protected void sendEmail() {
        String[] TO = {"jesusgalindohernadez@gmail.com"}; //aquí pon tu correo
        String[] CC = {""};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setData(Uri.EMPTY.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_CC, CC);
// Esto podrás modificarlo si quieres, el asunto y el cuerpo del mensaje
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Asunto");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Escribe aquí tu mensaje");

        try {
            startActivity(Intent.createChooser(emailIntent, "Enviar email..."));
            finish();
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(LoginActivity.this,
                    "No tienes clientes de email instalados.", Toast.LENGTH_SHORT).show();
        }
    }

    private void enviarEmail(){
        //Instanciamos un Intent del tipo ACTION_SEND
        Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
        //Aqui definimos la tipologia de datos del contenido dle Email en este caso text/html
        emailIntent.setType("text/html");
        // Indicamos con un Array de tipo String las direcciones de correo a las cuales enviar
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{"androdfast@gmail.com"});
        // Aqui definimos un titulo para el Email
        emailIntent.putExtra(android.content.Intent.EXTRA_TITLE, "El Titulo");
        // Aqui definimos un Asunto para el Email
        emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "El Asunto");
        // Aqui obtenemos la referencia al texto y lo pasamos al Email Intent
       // emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, getString(R.string.my_text));
        try {
            //Enviamos el Correo iniciando una nueva Activity con el emailIntent.
            startActivity(Intent.createChooser(emailIntent, "Enviar Correo..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(LoginActivity.this, "No hay ningun cliente de correo instalado.", Toast.LENGTH_SHORT).show();
        }
    }


    private class RestValidaBoleto extends AsyncTask<String,Void,PersonalSeguridadDTO> {


        @Override
        protected PersonalSeguridadDTO doInBackground(String... valida) {
            String link = "http://oitic.com.mx/boletera/api/concierto/valida/boleto";
            MediaType JSON = MediaType.parse("application/json; charset=utf-8");

            PersonalSeguridadDTO oPersonalSeguridad = null;
            OkHttpClient client = new OkHttpClient();

            String[] arrValor = valida[0].split("-");

            JSONObject jsonObject = new JSONObject();
            try { jsonObject.put("userName", valida[0]);
                jsonObject.put("contrasenna", valida   [1]);
            } catch (JSONException e)
            { e.printStackTrace();
            }

            RequestBody reqBody = RequestBody.create(JSON,jsonObject.toString());

            Request request = new Request.Builder()
                    .url(link)
                    .post(reqBody)
                    .build();
            Response response = null;
            try {
                response = client.newCall(request).execute();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Type personalSeguridadType = new TypeToken<PersonalSeguridadDTO>() {
            }.getType();
            try {
                String resultado=response.body().string();
                oPersonalSeguridad = new Gson().fromJson(resultado, personalSeguridadType);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return oPersonalSeguridad;
        }
    }






}
