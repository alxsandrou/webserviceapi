package movil.accesaa.oitic.com.mx.boletera.model;

import java.io.Serializable;

public class CodigoDTO implements Serializable {

    public CodigoDTO(){}

    private String qrCode;

    private Long idEvento;

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    public Long getIdEvento() {
        return idEvento;
    }

    public void setIdEvento(Long idEvento) {
        this.idEvento = idEvento;
    }
}
