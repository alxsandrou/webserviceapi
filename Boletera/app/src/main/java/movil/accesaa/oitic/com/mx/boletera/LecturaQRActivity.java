package movil.accesaa.oitic.com.mx.boletera;

import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.blikoon.qrcodescanner.QrCodeActivity;


public class LecturaQRActivity extends AppCompatActivity {
    public  TextView textView3,textView5;
    private static final int REQUEST_CODE_QR_SCAN = 101;
    private Button btnSalir;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lectura_qr);

        Intent i = new Intent(LecturaQRActivity.this, QrCodeActivity.class);
        startActivityForResult(i, REQUEST_CODE_QR_SCAN);
        btnSalir = findViewById(R.id.btnSalir);
        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
    }


    public void onClick(View v) {
        Intent i = new Intent(LecturaQRActivity.this, QrCodeActivity.class);
        startActivityForResult(i, REQUEST_CODE_QR_SCAN);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        /*if (resultCode != Activity.RESULT_OK) {
            Toast.makeText(getApplicationContext(), "No se pudo obtener una respuesta", Toast.LENGTH_SHORT).show();
            String resultado = data.getStringExtra("com.blikoon.qrcodescanner.error_decoding_image");
              if (resultado != null) {
                Toast.makeText(getApplicationContext(), "No se pudo escanear el código QR", Toast.LENGTH_SHORT).show();
            }
           return;
        } */
         if (requestCode == REQUEST_CODE_QR_SCAN) {
            if (data != null) {

               final String lectura = data.getStringExtra("com.blikoon.qrcodescanner.got_qr_scan_relult");

                Intent intent = new Intent(this,Boleto.class);
                intent.putExtra("qrCode",lectura);
                startActivityForResult(intent,0);
            }
        }
    }
}
