package movil.accesaa.oitic.com.mx.boletera;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;


import android.support.v7.widget.CardView;
import android.text.Html;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;

import movil.accesaa.oitic.com.mx.boletera.model.BoletoDTO;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Boleto extends AppCompatActivity {

    private TextView tvwTipoBoleto, tvwPrecio, tvwAsiento, tvwFromaPago,
            tvwBoleto, tvwFechaCompra, tvwFolio, tvwFila, tvwEstatus, tvwEstatusBoleto;
    private CardView CardView;
    private TableLayout tabla, tabla0;
    private CardView dvwValida;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_boleto);
        dvwValida = findViewById(R.id.dvwValida);

        Bundle bundle = getIntent().getExtras();
        final String dato = bundle.getString("qrCode");
        new RestBoleto().execute(dato);
        //new RestValidaBoleto().execute(dato);


        dvwValida.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new RestValidaBoleto().execute(dato);

                Intent intent = new Intent(v.getContext(),LecturaQRActivity.class);
                startActivityForResult(intent,0);
            }
        });
    }


    private class RestBoleto extends AsyncTask<String, Void, BoletoDTO> {
        @Override
        protected BoletoDTO doInBackground(String... qrCode) {
            //String link = "https://oitic.com.mx/boletera/api/concierto/valida/boleto";
            String link = "https://oitic.com.mx/boletera/api/concierto/valida/boleto";
            MediaType JSON = MediaType.parse("application/json; charset=utf-8");

            BoletoDTO oBoleto = null;

            OkHttpClient client = new OkHttpClient();


            String[] arrValor = qrCode[0].split("-");

            if(arrValor.length == 2) {


                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("idEvento", arrValor[0]);
                    jsonObject.put("qrCode", arrValor[1]);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                RequestBody reqBody = RequestBody.create(JSON, jsonObject.toString());

                Request request = new Request.Builder()
                        .url(link)
                        .post(reqBody)
                        .build();
                Response response = null;
                try {
                    response = client.newCall(request).execute();
                    Type boletoType = new TypeToken<BoletoDTO>() {
                    }.getType();


                    if (response.isSuccessful() && response.code() == 200 && response.message().equals("OK")) {
                        String resultado = response.body().string();
                        oBoleto = new Gson().fromJson(resultado, boletoType);
                        return oBoleto;
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                }

                // return null;

                return oBoleto;
            }else
                return null;

        }


        @Override
        protected void onPostExecute(BoletoDTO boletoDTO) {


            if (boletoDTO != null) {
                tvwTipoBoleto = findViewById(R.id.tvwTipoBoleto);
                tvwPrecio = findViewById(R.id.tvwPrecio);
                tvwAsiento = findViewById(R.id.tvwAsiento);
                tvwFromaPago = findViewById(R.id.tvwFromaPago);
                tvwBoleto = findViewById(R.id.tvwBoleto);
                tvwFechaCompra = findViewById(R.id.tvwFechaCompra);
                tvwFolio = findViewById(R.id.tvwFolio);
                tvwFila = findViewById(R.id.tvwFila);
                tvwEstatus = findViewById(R.id.tvwEstatus);
                tvwEstatusBoleto = findViewById(R.id.tvwEstatusBoleto);
                CardView = findViewById(R.id.dvwValida);
                tabla = findViewById(R.id.tabla);
                tabla0 = findViewById(R.id.tabla0);

                tvwTipoBoleto.setText(boletoDTO.getNombreTipoBoleto());
                tvwPrecio.setText(boletoDTO.getPrecio().toString());
                tvwAsiento.setText(boletoDTO.getIdAsiento().toString());
                tvwFromaPago.setText(boletoDTO.getIdFormaPago().toString());
                tvwBoleto.setText(boletoDTO.getIdBoleto().toString());
                tvwFechaCompra.setText(boletoDTO.getFechaCompra().toString());
                tvwFolio.setText(boletoDTO.getFolio().toString());
                tvwFila.setText(boletoDTO.getFila().toString());
                tvwEstatus.setText(boletoDTO.getNombreEstatusBoleto().toString());
                tvwEstatusBoleto.setText(boletoDTO.getFechaEstatusBoleto().toString());

                CardView.setVisibility(View.VISIBLE);
                //tabla.setBackgroundColor(Color.GREEN);

                if (boletoDTO.getIdEstatusBoleto() == 10) {
                    CardView.setVisibility(View.VISIBLE);
                    tabla.setBackgroundColor(Color.GREEN);

                } else if (boletoDTO.getIdEstatusBoleto() == 13) {
                    CardView.setVisibility(View.INVISIBLE);
                    tabla0.setVisibility(View.VISIBLE);
                    tabla0.setBackgroundColor(Color.RED);

                  /*  AlertDialog.Builder dialogo1 = new AlertDialog.Builder(Boleto.this);
                    dialogo1.setTitle("ALERTA");
                    dialogo1.setMessage("Boleto ya validado");
                    dialogo1.setCancelable(false);

                    dialogo1.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogo1, int id) {
                            cancelar();
                        }
                    });
                    dialogo1.show();  */

                }


            }else {
                AlertDialog.Builder dialogo1 = new AlertDialog.Builder(Boleto.this);
                //dialogo1.setIcon(android.R.drawable.ic_dialog_alert);
                dialogo1.setIcon(R.drawable.ic_alert);


                //dialogo1.setTitle("ALERTA");
                dialogo1.setTitle(Html.fromHtml("<font color='#B00020'>ALERTA</font>"));
                dialogo1.setMessage("BOLETO INVALIDO, LLAMAR A SEGURIDAD");
                dialogo1.setCancelable(false);

                dialogo1.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogo1, int id) {
                        cancelar();
                    }
                });
                dialogo1.show();
            }
        }
    }


    public void cancelar() {
        Intent myIntent = new Intent(Boleto.this, LecturaQRActivity.class);
        startActivity(myIntent);
        finish();
    }

    private class RestValidaBoleto extends AsyncTask<String, Void, Void> {
        @Override
        protected Void doInBackground(String... qrCode) {
            String link = "https://oitic.com.mx/boletera/api/concierto/registra/checkin";
           // String link = "https://oitic.com.mx/boletera/api/concierto/registra/checkin";
            MediaType JSON = MediaType.parse("application/json; charset=utf-8");


            OkHttpClient client = new OkHttpClient();
            String[] arrValor = qrCode[0].split("-");

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("idEvento", arrValor[0]);
                jsonObject.put("qrCode", arrValor[1]);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            RequestBody reqBody = RequestBody.create(JSON, jsonObject.toString());

            Request request = new Request.Builder()
                    .url(link)
                    .post(reqBody)
                    .build();
            Response response = null;
            try {
                response = client.newCall(request).execute();

                //if( response.isSuccessful() && response.code() == 200){
                String resultado = response.body().string();
                response.message();

                //}
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }
    }
}
