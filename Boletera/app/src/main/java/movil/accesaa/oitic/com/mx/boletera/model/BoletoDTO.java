package movil.accesaa.oitic.com.mx.boletera.model;


public class BoletoDTO {
    /**
     * Este codigo se genero con Arquitecto MVC.
     * Esta propiedad corresponde con el mapeo en base de datos
     * de la columna boleto.id_asiento
     *
     */

    private Long idAsiento;

    /**
     * Este codigo se genero con Arquitecto MVC.
     * Esta propiedad corresponde con el mapeo en base de datos
     * de la columna boleto.id_forma_pago
     *
     */

    private String idFormaPago;

    /**
     * Este codigo se genero con Arquitecto MVC.
     * Esta propiedad corresponde con el mapeo en base de datos
     * de la columna boleto.id_usuario
     *
     */

    private Long idUsuario;

    /**
     * Este codigo se genero con Arquitecto MVC.
     * Esta propiedad corresponde con el mapeo en base de datos
     * de la columna boleto.id_boleto
     *
     */

    private Long idBoleto;

    /**
     * Este codigo se genero con Arquitecto MVC.
     * Esta propiedad corresponde con el mapeo en base de datos
     * de la columna boleto.fecha_compra
     *
     */

    private String fechaCompra;

    /**
     * Este codigo se genero con Arquitecto MVC.
     * Esta propiedad corresponde con el mapeo en base de datos
     * de la columna boleto.cortesia
     *
     */

    private Boolean cortesia;

    /**
     * Este codigo se genero con Arquitecto MVC.
     * Esta propiedad corresponde con el mapeo en base de datos
     * de la columna boleto.precio
     *
     */

    private Double precio;

    /**
     * Este codigo se genero con Arquitecto MVC.
     * Esta propiedad corresponde con el mapeo en base de datos
     * de la columna asiento.folio
     *
     */

    private String folio;

    /**
     * Este codigo se genero con Arquitecto MVC.
     * Esta propiedad corresponde con el mapeo en base de datos
     * de la columna asiento.fila
     *
     */

    private String fila;

    /**
     * Este codigo se genero con Arquitecto MVC.
     * Esta propiedad corresponde con el mapeo en base de datos
     * de la columna tipo_boleto.id_tipo_boleto
     *
     */

    private Short idTipoBoleto;

    /**
     * Este codigo se genero con Arquitecto MVC.
     * Esta propiedad corresponde con el mapeo en base de datos
     * de la columna tipo_boleto.id_tipo_boleto
     *
     */

    private String nombreTipoBoleto;

    /**
     * Este codigo se genero con Arquitecto MVC.
     * Esta propiedad corresponde con el mapeo en base de datos
     * de la columna boleto_has_estatus_boleto.id_estatus_boleto
     *
     */

    private Short idEstatusBoleto;

    /**
     * Este codigo se genero con Arquitecto MVC.
     * Esta propiedad corresponde con el mapeo en base de datos
     * de la columna boleto_has_estatus_boleto.id_estatus_boleto
     *
     */

    private String nombreEstatusBoleto;



    private Long idEvento;
    /**
     * Este codigo se genero con Arquitecto MVC.
     * Esta propiedad corresponde con el mapeo en base de datos
     * de la columna boleto.fecha_compra
     *
     */

    private String fechaEstatusBoleto;

    public Long getIdAsiento() {
        return idAsiento;
    }

    public void setIdAsiento(Long idAsiento) {
        this.idAsiento = idAsiento;
    }

    public String getIdFormaPago() {
        return idFormaPago;
    }

    public void setIdFormaPago(String idFormaPago) {
        this.idFormaPago = idFormaPago;
    }

    public Long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Long getIdBoleto() {
        return idBoleto;
    }

    public void setIdBoleto(Long idBoleto) {
        this.idBoleto = idBoleto;
    }

    public String getFechaCompra() {
        return fechaCompra;
    }

    public void setFechaCompra(String fechaCompra) {
        this.fechaCompra = fechaCompra;
    }

    public Boolean getCortesia() {
        return cortesia;
    }

    public void setCortesia(Boolean cortesia) {
        this.cortesia = cortesia;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getFila() {
        return fila;
    }

    public void setFila(String fila) {
        this.fila = fila;
    }

    public Short getIdTipoBoleto() {
        return idTipoBoleto;
    }

    public void setIdTipoBoleto(Short idTipoBoleto) {
        this.idTipoBoleto = idTipoBoleto;
    }

    public String getNombreTipoBoleto() {
        return nombreTipoBoleto;
    }

    public void setNombreTipoBoleto(String nombreTipoBoleto) {
        this.nombreTipoBoleto = nombreTipoBoleto;
    }

    public Short getIdEstatusBoleto() {
        return idEstatusBoleto;
    }

    public void setIdEstatusBoleto(Short idEstatusBoleto) {
        this.idEstatusBoleto = idEstatusBoleto;
    }

    public String getNombreEstatusBoleto() {
        return nombreEstatusBoleto;
    }

    public void setNombreEstatusBoleto(String nombreEstatusBoleto) {
        this.nombreEstatusBoleto = nombreEstatusBoleto;
    }

    public Long getIdEvento() {
        return idEvento;
    }

    public void setIdEvento(Long idEvento) {
        this.idEvento = idEvento;
    }

    public String getFechaEstatusBoleto() {
        return fechaEstatusBoleto;
    }

    public void setFechaEstatusBoleto(String fechaEstatusBoleto) {
        this.fechaEstatusBoleto = fechaEstatusBoleto;
    }
}
