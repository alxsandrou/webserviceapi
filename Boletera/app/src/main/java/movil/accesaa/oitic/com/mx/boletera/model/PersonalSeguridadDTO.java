package movil.accesaa.oitic.com.mx.boletera.model;

import java.time.LocalDate;

public class PersonalSeguridadDTO {

    /**
     * Este codigo se genero con Arquitecto MVC.
     * Esta propiedad corresponde con el mapeo en base de datos
     * de la columna personal_seguridad.id_usuario
     *
     */
    private Long idUsuario;

    /**
     * Este codigo se genero con Arquitecto MVC.
     * Esta propiedad corresponde con el mapeo en base de datos
     * de la columna personal_seguridad.user_name
     *
     */
    private String userName;

    /**
     * Este codigo se genero con Arquitecto MVC.
     * Esta propiedad corresponde con el mapeo en base de datos
     * de la columna personal_seguridad.contrasenna
     *
     */
    private String contrasenna;

    /**
     * Este codigo se genero con Arquitecto MVC.
     * Esta propiedad corresponde con el mapeo en base de datos
     * de la columna personal_seguridad.fecha_creacion
     *
     */
    private java.time.LocalDate fechaCreacion;

    /**
     * Este codigo se genero con Arquitecto MVC.
     * Esta propiedad corresponde con el mapeo en base de datos
     * de la columna personal_seguridad.cuenta_no_expirada
     *
     */
    private Boolean cuentaNoExpirada;

    /**
     * Este codigo se genero con Arquitecto MVC.
     * Esta propiedad corresponde con el mapeo en base de datos
     * de la columna personal_seguridad.cuenta_no_bloqueada
     *
     */
    private Boolean cuentaNoBloqueada;

    /**
     * Este codigo se genero con Arquitecto MVC.
     * Esta propiedad corresponde con el mapeo en base de datos
     * de la columna personal_seguridad.credencial_no_expirada
     *
     */
    private Boolean credencialNoExpirada;

    /**
     * Este codigo se genero con Arquitecto MVC.
     * Esta propiedad corresponde con el mapeo en base de datos
     * de la columna personal_seguridad.hablitado
     *
     */
    private Boolean hablitado;

    /**
     * Este codigo se genero con Arquitecto MVC.
     * Esta propiedad corresponde con el mapeo en base de datos
     * de la columna personal_seguridad.intentos_fallidos
     *
     */
    private Short intentosFallidos;

    /**
     * Este codigo se genero con Arquitecto MVC.
     * Esta propiedad corresponde con el mapeo en base de datos
     * de la columna personal_seguridad.pregunta_secreta
     *
     */
    private String preguntaSecreta;

    /**
     * Este codigo se genero con Arquitecto MVC.
     * Esta propiedad corresponde con el mapeo en base de datos
     * de la columna personal_seguridad.respuesta_secreta
     *
     */
    private String respuestaSecreta;

    /**
     * Este codigo se genero con Arquitecto MVC.
     * Esta propiedad corresponde con el mapeo en base de datos
     * de la columna personal_seguridad.activo
     *
     */
    private Boolean activo;

    public Long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getContrasenna() {
        return contrasenna;
    }

    public void setContrasenna(String contrasenna) {
        this.contrasenna = contrasenna;
    }

    public LocalDate getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(LocalDate fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Boolean getCuentaNoExpirada() {
        return cuentaNoExpirada;
    }

    public void setCuentaNoExpirada(Boolean cuentaNoExpirada) {
        this.cuentaNoExpirada = cuentaNoExpirada;
    }

    public Boolean getCuentaNoBloqueada() {
        return cuentaNoBloqueada;
    }

    public void setCuentaNoBloqueada(Boolean cuentaNoBloqueada) {
        this.cuentaNoBloqueada = cuentaNoBloqueada;
    }

    public Boolean getCredencialNoExpirada() {
        return credencialNoExpirada;
    }

    public void setCredencialNoExpirada(Boolean credencialNoExpirada) {
        this.credencialNoExpirada = credencialNoExpirada;
    }

    public Boolean getHablitado() {
        return hablitado;
    }

    public void setHablitado(Boolean hablitado) {
        this.hablitado = hablitado;
    }

    public Short getIntentosFallidos() {
        return intentosFallidos;
    }

    public void setIntentosFallidos(Short intentosFallidos) {
        this.intentosFallidos = intentosFallidos;
    }

    public String getPreguntaSecreta() {
        return preguntaSecreta;
    }

    public void setPreguntaSecreta(String preguntaSecreta) {
        this.preguntaSecreta = preguntaSecreta;
    }

    public String getRespuestaSecreta() {
        return respuestaSecreta;
    }

    public void setRespuestaSecreta(String respuestaSecreta) {
        this.respuestaSecreta = respuestaSecreta;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }
}
