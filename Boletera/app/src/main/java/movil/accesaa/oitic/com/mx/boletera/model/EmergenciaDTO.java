package movil.accesaa.oitic.com.mx.boletera.model;

public class EmergenciaDTO {
    /**
     * Este codigo se genero con Arquitecto MVC.
     * Esta propiedad corresponde con el mapeo en base de datos
     * de la columna emergencia.id_emergencia
     *
     */
    private Long idEmergencia;

    /**
     * Este codigo se genero con Arquitecto MVC.
     * Esta propiedad corresponde con el mapeo en base de datos
     * de la columna emergencia.codigo_qr
     *
     */

    private String codigoQr;

    /**
     * Este codigo se genero con Arquitecto MVC.
     * Esta propiedad corresponde con el mapeo en base de datos
     * de la columna emergencia.primera
     *
     */

    private String primera;

    /**
     * Este codigo se genero con Arquitecto MVC.
     * Esta propiedad corresponde con el mapeo en base de datos
     * de la columna emergencia.segunda
     *
     */

    private String segunda;

    /**
     * Este codigo se genero con Arquitecto MVC.
     * Esta propiedad corresponde con el mapeo en base de datos
     * de la columna emergencia.impresiones
     *
     */

    private Boolean impresiones;

    public Long getIdEmergencia() {
        return idEmergencia;
    }

    public void setIdEmergencia(Long idEmergencia) {
        this.idEmergencia = idEmergencia;
    }

    public String getCodigoQr() {
        return codigoQr;
    }

    public void setCodigoQr(String codigoQr) {
        this.codigoQr = codigoQr;
    }

    public String getPrimera() {
        return primera;
    }

    public void setPrimera(String primera) {
        this.primera = primera;
    }

    public String getSegunda() {
        return segunda;
    }

    public void setSegunda(String segunda) {
        this.segunda = segunda;
    }

    public Boolean getImpresiones() {
        return impresiones;
    }

    public void setImpresiones(Boolean impresiones) {
        this.impresiones = impresiones;
    }
}
