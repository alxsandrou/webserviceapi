package com.example.webservice;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity {

    TextView txtName;
    TextView txtUsername;
    TextView txtEmail;
    TextView txtPhone;
    TextView txtWeb;
    Button Consult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtName = findViewById(R.id.txtName);
        txtUsername = findViewById(R.id.txtUsername);
        txtEmail = findViewById(R.id.txtEmail);
        txtPhone = findViewById(R.id.txtPhone);
        txtWeb = findViewById(R.id.txtWeb);
        Consult = findViewById(R.id.Consult);

        Consult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Instanciar Clase ClassConnection
                ClassConnection connection = new ClassConnection();
                try {
                    String response = connection.execute("https://jsonplaceholder.typicode.com/users").get();

                    //Leer Json
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.optJSONObject(5);

                    String name = jsonObject.getString("name");
                    String username = jsonObject.getString("username");
                    String email = jsonObject.getString("email");
                    String phone = jsonObject.getString("phone");
                    String website = jsonObject.getString("website");

                    //Mostrarlos en los controles del Activity
                    txtName.setText(name);
                    txtUsername.setText(username);
                    txtEmail.setText(email);
                    txtPhone.setText(phone);
                    txtWeb.setText(website);



                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
